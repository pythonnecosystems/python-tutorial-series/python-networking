# Python Networking: 소켓 프로그래밍의 기초 <sup>[1](#footnote_1)</sup>

> <font size="3">Python을 사용하여 네트워크 통신을 위한 소켓을 만들고 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./networking.md#intro)
1. [소켓은 무엇이며 어떻게 작동할까?](./networking.md#sec_02)
1. [Python에서 소켓을 만드는 방법](./networking.md#sec_03)
1. [소켓을 이용한 데이터 송수신 방법](./networking.md#sec_04)
1. [소켓 프로그래밍의 오류와 예외 처리 방법](./networking.md#sec_05)
1. [클라이언트-서버 통신을 위한 소켓 사용 방법](./networking.md#sec_06)
1. [P2P 통신을 위한 소켓 사용 방법](./networking.md#sec_07)
1. [비동기 및 동시 프로그래밍을 위한 소켓 사용 방법](./networking.md#sec_08)
1. [SSL/TLS로 안전한 통신을 위한 소켓 사용 방법](./networking.md#sec_09)
1. [요약](./networking.md#summary)

<a name="footnote_1">1</a>: [Python Tutorial 40 — Python Networking: Socket Programming Basics](https://levelup.gitconnected.com/python-tutorial-40-python-networking-socket-programming-basics-8b82426c8e44?sk=f6f98358ee30c6d2bdf60e9b3ecbe9c8)를 편역하였다.